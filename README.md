﻿Flask - Simple Example
======================
Ett grundläggande Flask-projekt som även använder SQLAlchemy, WTForms, Bootstrap och Moment.js. Förenklar management med Migrate och Script.

Kom igång
---------
Initiera databasen med:
```
python manage.py db init
```

För att skapa den första migreringen och även tillämpa den i din databas kör du följande. Detta gör du sedan varje gång du ändrat din databasmodell.
```
python manage.py db migrate -m "Initial migration."
python manage.py db upgrade
```

För att starta utvecklingsservern kör du följande kommando:
```
python manage.py runserver
```

För att testa nyheter och användare
---------
Skapa användare igenom /skapa_konto eller koden nedan.
öppna ett interaktivt fönster i mappen och skriv:

```
from app import models, db
u =  models.Member('Förnamn', 'Efternamn', 'Nick', '0000000000', 'Stad', 'Adress','1111111111','Användarnamn','Lösenord','emailadress', admin (True eller False), Moderator (True eller False))
db.session.add(u) #Lägger till användaren i db sessionen
db.session.commit() #postar commiten till databasen där den lagras
```

Skapa ett inlägg genom att öppna ett interaktivt fönster i mappen och skriv:

Edit: Inlägg kan nu skapas igenom webbgränssnittet på /skapa_nyhetsinlagg, men se till att ha en användarinstans i db först.
```
from app import models, db
models.Member.query.all() #listar alla användare
u = models.Member.query.get(1) #plockar första objektinstansen
p = models.News(title='', textentry='', author=u) #skapa nyhetsinlägget
db.session.add(p) #Lägger till nyheten i db sessionen
db.session.commit() #postar commiten till databasen där den lagras
```